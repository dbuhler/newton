﻿using System;
using System.Collections.Generic;

namespace NewtonEngine.Matrices
{
    /// <summary>
    /// This class represents a 3D vector and supports common vector operations.
    /// </summary>
    public class Vector
    {
        #region Fields

        /// <summary>
        /// A static null vector.
        /// </summary>
        public static Vector NULL { get { return new Vector(); } }

        // The vector's dimension.
        private const int SIZE = 3;

        // The vector's components.
        private double[] values;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new null vector.
        /// </summary>
        public Vector()
        {
            values = new double[SIZE];
        }

        /// <summary>
        /// Creates a new vector with the given components (x, y, z).
        /// </summary>
        /// <param name="x">The vector's x-component.</param>
        /// <param name="y">The vector's y-component.</param>
        /// <param name="z">The vector's z-component.</param>
        public Vector(double x, double y, double z) : this()
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Creates a new vector with the given components.
        /// </summary>
        /// <param name="values">The vector's (x, y, z) components.</param>
        /// <exception cref="ArgumentException">values.Length is not 3.</exception>
        public Vector(double[] values)
        {
            if (values.Length != SIZE)
            {
                throw new ArgumentException();
            }

            this.values = values;
        }

        /// <summary>
        /// Creates a new vector from the given 1-by-4 matrix.
        /// </summary>
        /// <param name="m">The matrix containting the (x, y, z) components.</param>
        /// <exception cref="ArgumentException">m.NumRows is not 1 or m.NumCols is not 4.
        /// </exception>
        public Vector(Matrix m) : this()
        {
            if (m.NumRows != 1 || m.NumCols != SIZE + 1)
            {
                throw new ArgumentException();
            }
            
            X = m[0, 0];
            Y = m[0, 1];
            Z = m[0, 2];
        }

        #endregion

        #region Properties

        /// <summary>
        /// The vector's x-component.
        /// </summary>
        public double X
        {
            get { return values[0];  }
            set { values[0] = value; }
        }

        /// <summary>
        /// The vector's y-component.
        /// </summary>
        public double Y
        {
            get { return values[1];  }
            set { values[1] = value; }
        }

        /// <summary>
        /// The vector's z-component.
        /// </summary>
        public double Z
        {
            get { return values[2];  }
            set { values[2] = value; }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Accesses the vector's i-th component.
        /// </summary>
        /// <param name="i">The index of the component to access.</param>
        /// <returns>The component at position i.</returns>
        public double this[int i]
        {
            get { return values[i];  }
            set { values[i] = value; }
        }

        /// <summary>
        /// Adds the given two vectors v1 and v2 and returns their sum.
        /// </summary>
        /// <param name="v1">The left vector to add.</param>
        /// <param name="v2">The right vector to add.</param>
        /// <returns>The sum of v1 and v2.</returns>
        public static Vector operator +(Vector v1, Vector v2)
        {
            return new Vector(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        /// <summary>
        /// Subtracts the given vectors v1 and v2 and returns their difference.
        /// </summary>
        /// <param name="v1">The left vector to subtract.</param>
        /// <param name="v2">The right vector to subtract.</param>
        /// <returns>The difference of v1 and v2.</returns>
        public static Vector operator -(Vector v1, Vector v2)
        {
            return new Vector(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        /// <summary>
        /// Returns the vector.
        /// </summary>
        /// <param name="v">The vector to apply the unary plus to.</param>
        /// <returns>The vector v.</returns>
        public static Vector operator +(Vector v)
        {
            return v;
        }

        /// <summary>
        /// Flips the sign of all components and returns the result.
        /// </summary>
        /// <param name="v">The vector to apply the unary minus to.</param>
        /// <returns>The additive inverse of v.</returns>
        public static Vector operator -(Vector v)
        {
            return new Vector(-v.X, -v.Y, -v.Z);
        }

        /// <summary>
        /// Multiplies the given vector by the given scalar and returns the result.
        /// </summary>
        /// <param name="scalar">The scalar to multiply the vector by.</param>
        /// <param name="v">The vector to be multiplied.</param>
        /// <returns>The vector multiplied by the scalar.</returns>
        public static Vector operator *(double scalar, Vector v)
        {
            return new Vector(scalar * v.X, scalar * v.Y, scalar * v.Z);
        }

        /// <summary>
        /// Multiplies the given vector by the given scalar and returns the result.
        /// </summary>
        /// <param name="v">The vector to be multiplied.</param>
        /// <param name="scalar">The scalar to multiply the vector by.</param>
        /// <returns>The vector multiplied by the scalar.</returns>
        public static Vector operator *(Vector v, double scalar)
        {
            return scalar * v;
        }

        /// <summary>
        /// Divides the given vector by the given scalar and returns the result.
        /// </summary>
        /// <param name="v">The vector to be divided.</param>
        /// <param name="scalar">The scalar to dvide the vector by.</param>
        /// <returns>The vector divided by the scalar.</returns>
        public static Vector operator /(Vector v, double scalar)
        {
            return (1.0 / scalar) * v;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates and returns the dot product of the given two vectors.
        /// </summary>
        /// <param name="v1">The left vector of the dot product.</param>
        /// <param name="v2">The right vector of the dot product.</param>
        /// <returns>The dot product of v1 and v2.</returns>
        public static double DotProduct(Vector v1, Vector v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        /// <summary>
        /// Calculates and returns the cross product of the given two vectors.
        /// </summary>
        /// <param name="v1">The left vector of the cross product.</param>
        /// <param name="v2">The right vector of the cross product.</param>
        /// <returns>The cross product of v1 and v2.</returns>
        public static Vector CrossProduct(Vector v1, Vector v2)
        {
            return new Vector(
                v1.Y * v2.Z - v2.Y * v1.Z,
                v1.Z * v2.X - v2.Z * v1.X,
                v1.X * v2.Y - v2.X * v1.Y);
        }

        /// <summary>
        /// Check and returns whether the vector is a null vector.
        /// </summary>
        /// <returns>Whether the vector is a null vector.</returns>
        public bool IsNull()
        {
            return X == 0.0 && Y == 0.0 && Z == 0.0;
        }

        /// <summary>
        /// Creates and returns a normalized copy of the vector. Does not affect the null vector.
        /// </summary>
        /// <returns>A normalized copy of the vector (unless it's the null vector).</returns>
        public Vector Normalize()
        {
            if (IsNull())
            {
                return Vector.NULL;
            }
            
            return this / GetLength();
        }

        /// <summary>
        /// Calculates and returns the the vector's magnitude (length). Note that this call is
        /// computationally more intensive than GetLengthSquare() and should be used only in cases
        /// where GetLengthSquare() does not suffice.
        /// </summary>
        /// <returns>The vector's magnitude.</returns>
        public double GetLength()
        {
            return Math.Sqrt(GetLengthSquare());
        }

        /// <summary>
        /// Calculates and returns the square of the vector's magnitude (length).
        /// </summary>
        /// <returns>The vector's maginuted squared.</returns>
        public double GetLengthSquare()
        {
            return Vector.DotProduct(this, this);
        }

        /// <summary>
        /// Returns an array with the vector's components.
        /// </summary>
        /// <returns>The vector's components.</returns>
        public double[] ToArray()
        {
            return (double[]) values.Clone();
        }

        /// <summary>
        /// Returns the vector as a 1-by-4 matrix with the given value as the 4th entry.
        /// </summary>
        /// <param name="w">The homogeneous component for the 1-by-4 matrix.</param>
        /// <returns>The vector as a 1-by-4 matrix.</returns>
        public Matrix ToMatrix(double w = 0)
        {
            return new Matrix(new double[,] { { X, Y, Z, w } });
        }

        /// <summary>
        /// Returns the vector's components as a string "(x, y, z)".
        /// </summary>
        /// <returns>A string representation of the vector.</returns>
        public override string ToString()
        {
            return String.Format("({0:F1}, {1:F1}, {2:F1})", X, Y, Z);
        }

        /// <summary>
        /// Creates a list of vectors from the rows of the given matrix.
        /// </summary>
        /// <param name="m">The matrix containting the vectors.</param>
        /// <returns>A list of vectors extracted from m.</returns>
        /// <exception cref="ArgumentException">m.NumCols is not 4.</exception>
        public static IList<Vector> ListFromMatrix(Matrix m)
        {
            if (m.NumCols != SIZE + 1)
            {
                throw new ArgumentException();
            }
            
            List<Vector> vectors = new List<Vector>();

            for (int i = 0; i < m.NumRows; ++i)
            {
                vectors.Add(new Vector(m.GetRow(i)));
            }

            return vectors;
        }

        #endregion
    }
}