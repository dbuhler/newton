﻿using NewtonEngine.Matrices;
using System;

namespace NewtonEngine.Objects
{
    /// <summary>
    /// This class represents a sphere defined by a radius.
    /// </summary>
    public class Sphere : Collidable
    {
        #region Fields

        /// <summary>
        /// The drag coefficient of a sphere.
        /// </summary>
        public const double DRAG_COEFFICIENT = 0.47;

        private Matrix boundingBoxVertices;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new sphere at position (x, y, z) with the specified radius.
        /// </summary>
        /// <param name="x">The x-coordinate of the sphere's position.</param>
        /// <param name="y">The y-coordinate of the sphere's position.</param>
        /// <param name="z">The z-coordinate of the sphere's position.</param>
        /// <param name="radius">The sphere's radius (in m).</param>
        public Sphere(double x, double y, double z, double radius) : base(x, y, z)
        {
            boundingBoxVertices = new Matrix(8, 4);
            boundingBoxVertices.SetRow(0, -1, -1, -1, 1);
            boundingBoxVertices.SetRow(1, -1, -1, +1, 1);
            boundingBoxVertices.SetRow(2, -1, +1, -1, 1);
            boundingBoxVertices.SetRow(3, -1, +1, +1, 1);
            boundingBoxVertices.SetRow(4, +1, -1, -1, 1);
            boundingBoxVertices.SetRow(5, +1, -1, +1, 1);
            boundingBoxVertices.SetRow(6, +1, +1, -1, 1);
            boundingBoxVertices.SetRow(7, +1, +1, +1, 1);

            Scale(radius);
        }

        #endregion

        #region Properties

        /// <summary>
        /// The sphere's radius (in m).
        /// </summary>
        public override double Radius { get { return Scaling.X; } }

        /// <summary>
        /// The area of the sphere's largest cross section (in m^2).
        /// </summary>
        public double CrossSectionArea { get { return Math.PI * this.Radius * this.Radius; } }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates and returns the matrix of vertices of the sphere's bounding box.
        /// </summary>
        /// <returns>The vertices of the sphere's bounding box.</returns>
        public override Matrix GetBoundingBox()
        {
           return boundingBoxVertices
                  * Transformations.Scaling(Radius)
                  * Transformations.Translation(Position);
        }

        /// <summary>
        /// Calculates and returns the sphere's volume in m^3.
        /// </summary>
        /// <returns>The sphere's volume in m^3.</returns>
        public override double GetVolume()
        {
            return 4.0 * Math.PI * Radius * Radius * Radius / 3.0;
        }

        /// <summary>
        /// Calculates and returns the normalized normal vector of the sphere's surface towards the
        /// given position.
        /// </summary>
        /// <param name="position">The position to get the normal vector to.</param>
        /// <returns>The normalized normal vector of the surface towards the position.</returns>
        public override Vector GetNormalTo(Vector position)
        {
            return (position - Position).Normalize();
        }

        /// <summary>
        /// Scales the sphere by (sx, sy, sz) if sx = sy = sz. Otherwise, an exception is thrown.
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        /// <exception cref="NotSupportedException">If sx != sy != sz.</exception>
        public override void Scale(double sx, double sy, double sz)
        {
            if (sx != sy && sy != sz)
            {
                throw new NotSupportedException();
            }
            else
            {
                base.Scale(sx, sy, sz);
            }
        }

        #endregion
    }
}