﻿using NewtonEngine.Objects;
using NewtonLab.Graphics;
using SharpDX;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Matrix = NewtonEngine.Matrices.Matrix;
using Matrix11 = SharpDX.Matrix;

namespace NewtonLab
{
    class ObjectPicker
    {
        struct Face
        {
            public Face(Vector4 p1, Vector4 p2)
            {
                point1 = p1;
                point2 = p2;

                /* Check which axis it is aligned with */
                if (p1.X == p2.X)
                {
                    AxisAligned = Axis.YZ;
                    AlignedValue = p1.X;
                }
                else if (p1.Y == p2.Y)
                {
                    AxisAligned = Axis.XZ;
                    AlignedValue = p1.Y;
                }
                else if (p1.Z == p2.Z)
                {

                    AxisAligned = Axis.XY;
                    AlignedValue = p1.Z;
                }
                else
                {
                    AxisAligned = Axis.None;
                    AlignedValue = 0;
                }
            }

            public enum Axis { None, XY, YZ, XZ }

            public float AlignedValue;

            public Axis AxisAligned;

            public Vector4 point1;
            public Vector4 point2;
        }

        public GraphicsHandler GraphicsHandler { get; private set; }

        /// <summary>
        /// Using ray to try to pick objects.
        /// </summary>
        public GameObject PickingObjects(RenderForm form, Camera camera, float screenWidth, float screenHeight, GraphicsHandler gHandler)
        {
            GraphicsHandler = gHandler;

            List<GameObject> objectsInFront = GameObject.GetObjectsInFromt(camera);
            System.Drawing.Point point = form.PointToClient(new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y));
            Vector2 clickPoint = new Vector2(point.X, point.Y);
            Matrix11 view;
            Vector3 ray;

            clickPoint.X = ((2.0f * (float)clickPoint.X) / screenWidth) - 1.0f;
            clickPoint.Y = (((2.0f * (float)clickPoint.Y) / screenHeight) - 1.0f) * -1.0f;

            /* Project the clicked point into 3D space */
            clickPoint = ProjectClickPoint(clickPoint);

            /* Invert the view matrix */
            view = camera.View;
            view.Invert();

            /* Calculate the ray vector */
            ray.X = (clickPoint.X * view.M11) + (clickPoint.Y * view.M21) + view.M31;
            ray.Y = (clickPoint.X * view.M12) + (clickPoint.Y * view.M22) + view.M32;
            ray.Z = (clickPoint.X * view.M13) + (clickPoint.Y * view.M23) + view.M33;

            /* Create list to store list of picked to find closest pick later */
            List<GameObject> clicked = new List<GameObject>();

            foreach (GameObject gameObject in objectsInFront)
            {
                /* Get world matrix from the location of the model */
                Matrix11 world = Matrix11.Translation(gameObject.Position.X, gameObject.Position.Y, gameObject.Position.Z);

                /* Inverse for "into screen */
                world.Invert();

                /* From view space to world space */
                Vector3 rayOrigin = Vector3.TransformCoordinate(camera.Position, world);
                ray = Vector3.TransformNormal(ray, world);

                /* Normalize ray vector */
                ray.Normalize();

                if (gameObject.PhysicsObject is Sphere)
                {
                    if (SphereIntersectionTest(rayOrigin, ray, (gameObject.PhysicsObject as Sphere).Radius))
                    {
                        clicked.Add(gameObject);
                    }
                }
                else if (gameObject.PhysicsObject is Block)
                {
                    /* Align camera for AABB testing */
                    rayOrigin = camera.Position;

                    if (BlockIntersectionTest(rayOrigin, ray, gameObject.PhysicsObject as Block))
                    {
                        clicked.Add(gameObject);
                    }
                }
            }

            /* Find closest picked */
            if (clicked.Count > 0)
            {
                /* Store the closest game object */
                GameObject closest = clicked[0];

                foreach (GameObject gameObject in clicked)
                {
                    /* If the distance of this game object to camera is shorter than the closest object to the camera, new closest */
                    if (Vector3.Distance(gameObject.Position, camera.Position) < Vector3.Distance(closest.Position, camera.Position))
                    {
                        closest = gameObject;
                    }
                }

                return closest;
            }

            return null;
        }

        private bool SphereIntersectionTest(Vector3 rayOrigin, Vector3 rayDirection, double radius)
        {
            /* Crazy Math stuff that I don't understand */
            double a, b, c, discriminant;

            /* Calculate the a, b, and c coefficients */
            a = (rayDirection.X * rayDirection.X) + (rayDirection.Y * rayDirection.Y) + (rayDirection.Z * rayDirection.Z);
            b = ((rayDirection.X * rayOrigin.X) + (rayDirection.Y * rayOrigin.Y) + (rayDirection.Z * rayOrigin.Z)) * 2.0f;
            c = ((rayOrigin.X * rayOrigin.X) + (rayOrigin.Y * rayOrigin.Y) + (rayOrigin.Z * rayOrigin.Z)) - (radius * radius);

            /* Find the discriminant.*/
            discriminant = (b * b) - (4 * a * c);

            /* if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.*/
            if (discriminant < 0.0f)
            {
                return false;
            }

            return true;
        }

        private bool BlockIntersectionTest(Vector3 rayOrigin, Vector3 rayDirection, Block block)
        {
            /* Get block vertices */
            Matrix11 blockMatrix1 = CopyToDX11Matrix(block.GetBoundingBox(), 0);
            Matrix11 blockMatrix2 = CopyToDX11Matrix(block.GetBoundingBox(), 4);

            /* Copy Transformation into DX11 Matrix */
            Matrix11 blockT = CopyToDX11Matrix(block.GetTransformation(), 0);
            Matrix11 inverseBlockT = CopyToDX11Matrix(block.GetTransformation(), 0);

            /* Invert the matrix */
            inverseBlockT.Invert();

            /* Translate block to origin */
            blockMatrix1 *= inverseBlockT;
            blockMatrix2 *= inverseBlockT;

            /* Translate ray */
            Vector3 rayOrigin4 = Vector3.TransformCoordinate(rayOrigin, inverseBlockT);
            Vector3 rayDirection4 = Vector3.TransformNormal(rayDirection, inverseBlockT);            

            List<Face> faces = CreateFaces(blockMatrix1, blockMatrix2);

            return TestFaceIntersection(rayOrigin4, rayDirection4, faces);
        }

        private List<Face> CreateFaces(Matrix11 blockMatrix1, Matrix11 blockMatrix2)
        {
            List<Face> faces = new List<Face>();

            /* Create faces */
            faces.Add(new Face(blockMatrix2.Row1, blockMatrix2.Row4));
            faces.Add(new Face(blockMatrix2.Row2, blockMatrix1.Row4));
            faces.Add(new Face(blockMatrix1.Row2, blockMatrix1.Row3));
            faces.Add(new Face(blockMatrix1.Row1, blockMatrix2.Row3));
            faces.Add(new Face(blockMatrix2.Row1, blockMatrix1.Row2));
            faces.Add(new Face(blockMatrix1.Row3, blockMatrix2.Row4));

            return faces;
        }

        private bool TestFaceIntersection(Vector3 rayOrigin, Vector3 rayDirection, List<Face> faces)
        {
            foreach (Face face in faces)
            {
                float t, x, y, z, minX, maxX, minY, maxY, minZ, maxZ;
                switch (face.AxisAligned)
                {
                    case Face.Axis.YZ:
                        /* Find t */
                        rayDirection.Normalize();
                        t = (face.AlignedValue - rayOrigin.X) / rayDirection.X;

                        /* Find y and z values */
                        y = rayOrigin.Y + t * rayDirection.Y;
                        z = rayOrigin.Z + t * rayDirection.Z;

                        /* Find minimum and maximum coordinates */
                        minY = Math.Min(face.point1.Y, face.point2.Y);
                        maxY = Math.Max(face.point1.Y, face.point2.Y);
                        minZ = Math.Min(face.point1.Z, face.point2.Z);
                        maxZ = Math.Max(face.point1.Z, face.point2.Z);

                        /* Compare y and z */
                        if (y >= minY && y <= maxY)
                        {
                            if (z >= minZ && z <= maxZ)
                            {
                                return true;
                            }
                        }
                        break;
                    case Face.Axis.XY:
                        /* Find t */
                        rayDirection.Normalize();
                        t = (face.AlignedValue - rayOrigin.Z) / rayDirection.Z;

                        /* Find x and y values */
                        x = rayOrigin.X + t * rayDirection.X;
                        y = rayOrigin.Y + t * rayDirection.Y;

                        /* Find minimum and maximum coordinates */
                        minX = Math.Min(face.point1.X, face.point2.X);
                        maxX = Math.Max(face.point1.X, face.point2.X);
                        minY = Math.Min(face.point1.Y, face.point2.Y);
                        maxY = Math.Max(face.point1.Y, face.point2.Y);

                        /* Compare x and y */
                        if (x >= minX && x <= maxX)
                        {
                            if (y >= minY && y <= maxY)
                            {
                                return true;
                            }
                        }
                        break;
                    case Face.Axis.XZ:
                        /* Find t */
                        rayDirection.Normalize();
                        t = (face.AlignedValue - rayOrigin.Y) / rayDirection.Y;

                        /* Find x and z values */
                        x = rayOrigin.X + t * rayDirection.X;
                        z = rayOrigin.Z + t * rayDirection.Z;

                        /* Find minimum and maximum coordinates */
                        minX = Math.Min(face.point1.X, face.point2.X);
                        maxX = Math.Max(face.point1.X, face.point2.X);
                        minZ = Math.Min(face.point1.Z, face.point2.Z);
                        maxZ = Math.Max(face.point1.Z, face.point2.Z);


                        /* Compare x and z */
                        if (x >= minX && x <= maxX)
                        {
                            if (z >= minZ && z <= maxZ)
                            {
                                return true;
                            }
                        }
                        break;
                }
            }

            return false;
        }

        /// <summary>
        /// Copies a Newton Lab matrix to a DirectX11 Matrix.
        /// </summary>
        /// <param name="matrix">The matrix to copy. </param>
        /// <param name="startingRow">The row to start copying from. </param>
        /// <returns> Returns the matrix that has been copied to. </returns>
        private Matrix11 CopyToDX11Matrix(Matrix matrix, int startingRow)
        {
            Matrix11 newMatrix = new Matrix11();
            double[] row = matrix.GetRow(startingRow);
            newMatrix.Row1 = new Vector4((float)row[0], (float)row[1], (float)row[2], (float)row[3]);
            ++startingRow;

            row = matrix.GetRow(startingRow);
            newMatrix.Row2 = new Vector4((float)row[0], (float)row[1], (float)row[2], (float)row[3]);
            ++startingRow;

            row = matrix.GetRow(startingRow);
            newMatrix.Row3 = new Vector4((float)row[0], (float)row[1], (float)row[2], (float)row[3]);
            ++startingRow;

            row = matrix.GetRow(startingRow);
            newMatrix.Row4 = new Vector4((float)row[0], (float)row[1], (float)row[2], (float)row[3]);

            return newMatrix;
        }

        private Vector2 ProjectClickPoint(Vector2 clickPoint)
        {
            clickPoint.X = clickPoint.X / GraphicsHandler.Projection.M11;
            clickPoint.Y = clickPoint.Y / GraphicsHandler.Projection.M22;

            return clickPoint;
        }
    }
}
