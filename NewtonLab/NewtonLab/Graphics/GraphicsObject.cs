﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using SharpDX.Direct3D;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using Buffer11 = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

using NewtonLab.Graphics.ObjImport;

using NewtonEngine.Objects;
using NewtonEngine;
using NewtonEngine.Matrices;

using SphereType = NewtonLab.GameObject.SphereType;
using BlockType = NewtonLab.GameObject.BlockType;

namespace NewtonLab.Graphics
{
    class GraphicsObject
    {
        /* Defaults */
        public static GraphicsHandler DefaultGraphicsHandler;
        public static PhysicsEngine DefaultPhysicsEngine;

        #region Properties
        public GraphicsHandler GraphicsHandler { get; protected set; }
        public PhysicsEngine NewtonEngine { get; protected set; }
        public Device Device { get; protected set; }
        public List<GraphicsObjectPart> Parts { get; protected set; }
        /// <summary>
        /// Vertex Buffer
        /// </summary>
        public Buffer11 VertexBuffer { get; protected set; }
        /// <summary>
        /// Index Buffer
        /// </summary>
        public Buffer11 IndexBuffer { get; protected set; }
        /// <summary>
        /// Vertex Size
        /// </summary>
        public int VertexSize { get; protected set; }
        public PhysicsObject PhysicsObject { get; protected set; }
        public List<StaticVertex> Vertices { get; protected set; }
        public List<int> Indices { get; protected set; }
        public float[] Transformations { get { return PhysicsObject.GetTransformation().ToFloatArray(); } }
        public bool Highlighted { get { return PhysicsObject.Highlighted; } }
        public double Mass { get { return PhysicsObject.Mass;} set {PhysicsObject.Mass = value;} }
        public Vector3 Position
        {
            get
            {
                return new Vector3((float)PhysicsObject.Position.X, (float)PhysicsObject.Position.Y, (float)PhysicsObject.Position.Z);
            }
            set { PhysicsObject.Position = new Vector(value.X, value.Y, value.Z); }
        }
        public bool Fixed{ get{ return PhysicsObject.Fixed; } set{ PhysicsObject.Fixed = value;} }
        public Vector3 Velocity {
            get { return new Vector3((float)PhysicsObject.Velocity.X, (float)PhysicsObject.Velocity.Y, (float)PhysicsObject.Velocity.Z); }
            set { PhysicsObject.Velocity = new Vector(value.X, value.Y, value.Z); }
        }
        #endregion

        /* Model Reader */
        private static ObjReader objReader = new ObjReader();

        #region Constructors
        /// <summary>
        /// Default constructor. Only used by child classes for factory.
        /// </summary>

        protected GraphicsObject()

        {

        }

        /// <summary>
        /// Creates a copy of the input Graphics Object.
        /// </summary>
        /// <param name="g">The Graphics Object to copy. </param>
        public GraphicsObject(GraphicsObject g)
        {
            GraphicsHandler = g.GraphicsHandler;

            /* Check  type to create */
            if (g.PhysicsObject is Block)
            {
                PhysicsObject = new Block(
                    g.Position.X, g.Position.Y, g.Position.Z,
                    g.PhysicsObject.Scaling.X,
                    g.PhysicsObject.Scaling.Y,
                    g.PhysicsObject.Scaling.Z);
            }
            else if (g.PhysicsObject is Sphere)
            {
                PhysicsObject = new Sphere(
                    g.Position.X, g.Position.Y, g.Position.Z,
                    (g.PhysicsObject as Sphere).Radius);
            }
            else if (g.PhysicsObject is PhysicsObject)
            {
                PhysicsObject = new PhysicsObject(g.Position.X, g.Position.Y, g.Position.Z);
            }

            Parts = new List<GraphicsObjectPart>(g.Parts);

            Vertices = g.Vertices;
            Indices = g.Indices;

            VertexBuffer = Buffer11.Create<StaticVertex>(g.Device, BindFlags.VertexBuffer, g.Vertices.ToArray());
            IndexBuffer = Buffer11.Create(g.Device, BindFlags.IndexBuffer, g.Indices.ToArray());
            VertexSize = SharpDX.Utilities.SizeOf<StaticVertex>(); 
        }
        #endregion

        #region Factories

        /// <summary>
        /// Creates a Graphics Object that contains a Sphere Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="radius">The initial radius of the Graphics Object. </param>
        /// <param name="sphereType">The type of sphere to spawn. </param>
        /// <param name="gHandler">The Graphics Handler that will render this Graphics Object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this Graphics Object. Optional. </param>
        /// <returns> Returns a Graphics Object that contains a Sphere Physics Object. </returns>
        protected static GraphicsObject CreateSphere(Vector3 position, double radius, SphereType sphereType = SphereType.Normal,  GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {
            /* Create an empty graphics object to fill */
            GraphicsObject newObject = new GraphicsObject();

            /* Set Graphics Handler and Physics Engine when set */
            newObject.GraphicsHandler = gHandler ?? DefaultGraphicsHandler;
            newObject.NewtonEngine = nEngine ?? DefaultPhysicsEngine;
            newObject.Device = newObject.GraphicsHandler.D3DHandler.Device;

            switch (sphereType)
            {
                case SphereType.Normal:
                    /* Load the model data */
                    newObject.LoadModelFromFile("./Models/Primitives/sphere.obj");
                    break;
                case SphereType.Tennisball:
                    /* Load the model data */
                    newObject.LoadModelFromFile("./Models/tennisball/tennisball.obj");
                    break;
                case SphereType.Basketball:
                    newObject.LoadModelFromFile("./Models/basketball/basketball.obj");
                    break;
            }
            

            /* Create the Physics Object, in this case a Sphere */
            newObject.PhysicsObject = new Sphere(position.X, position.Y, position.Z, radius);

            return newObject;
        }


        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="dimensions">The initial dimensions of the Graphics Object. </param>
        /// <param name="gHandler">The Graphics Handler that will render this Graphics Object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this Graphics Object. Optional. </param>
        /// <returns> Returns a Graphics Object that contains a Block Physics Object. </returns>
        protected static GraphicsObject CreateBlock(Vector3 position, double[] dimensions, BlockType blockType = BlockType.Normal, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)

        {
            /* Create an empty graphics object to fill */
            GraphicsObject newObject = new GraphicsObject();

            /* Set Graphics Handler and Physics Engine when set */
            newObject.GraphicsHandler = gHandler ?? DefaultGraphicsHandler;
            newObject.NewtonEngine = nEngine ?? DefaultPhysicsEngine;
            newObject.Device = newObject.GraphicsHandler.D3DHandler.Device;

            switch (blockType)
            {
                case BlockType.Normal:
                    /* Load the model data */
                    newObject.LoadModelFromFile("./Models/Primitives/cube.obj");
                    break;
                case BlockType.Raccoon:
                    /* Load the model data */
                    newObject.LoadModelFromFile("./Models/raccoonblock/raccoonblock.obj");
                    break;
                case BlockType.BrickWall:
                    newObject.LoadModelFromFile("./Models/wall/wall.obj");
                    break;
                case BlockType.Floor:
                    newObject.LoadModelFromFile("./Models/hardwood_floor/floor.obj");
                    break;
            }

            /* Create the Physics Object, in this case a Sphere */
            switch(dimensions.Length)
            {
                case 1:
                    newObject.PhysicsObject = new Block(position.X, position.Y, position.Z, dimensions[0]);
                    break;
                case 3:
                    newObject.PhysicsObject = new Block(position.X, position.Y, position.Z, dimensions[0], dimensions[1], dimensions[2]);
                    break;
                default:
                    throw new ArgumentException();
            }

            return newObject;
        }

        /// <summary>
        /// Creates a Graphics Object that contains a Generic Physics Object.
        /// </summary>
        /// <param name="position">The initial position of the Physics Object. </param>
        /// <param name="filepath">The 3D model to load for this object </param>
        /// <param name="gHandler">The Graphics Handler that will render this Graphics Object. Optional. </param>
        /// <param name="nEngine">The Physics Engine that will update this Graphics Object. Optional. </param>
        /// <returns> Returns a Graphics Object that contains a Generic Physics Object. </returns>
        protected static GraphicsObject CreateObject(Vector3 position, string filepath, GraphicsHandler gHandler = null, PhysicsEngine nEngine = null)
        {
            /* Create an empty graphics object to fill */
            GraphicsObject newObject = new GraphicsObject();

            /* Set Graphics Handler and Physics Engine when set */
            newObject.GraphicsHandler = gHandler ?? DefaultGraphicsHandler;
            newObject.NewtonEngine = nEngine ?? DefaultPhysicsEngine;
            newObject.Device = newObject.GraphicsHandler.D3DHandler.Device;

            /* Load the model data */
            newObject.LoadModelFromFile(filepath);

            /* Create the Physics Object, in this case a Sphere */
            newObject.PhysicsObject = new PhysicsObject(position.X, position.Y, position.Z);

            return newObject;
        }

        #endregion

        #region Rendering Methods
        /// <summary>
        /// Preps the vertex and index buffers for drawing.
        /// </summary>
        public void Begin()
        {
            GraphicsHandler.D3DHandler.DeviceContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            GraphicsHandler.D3DHandler.DeviceContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(VertexBuffer, VertexSize, 0));
            GraphicsHandler.D3DHandler.DeviceContext.InputAssembler.SetIndexBuffer(IndexBuffer, Format.R32_UInt, 0);
        }

        /// <summary>
        /// Draw the specific part of the model.
        /// </summary>
        /// <param name="partIndex">The index of the part to be drawn. </param>
        public void Draw(int partIndex)
        {
            if (Parts[partIndex] is ConvexHull)
            {
                GraphicsHandler.D3DHandler.SetWireframeRasterState();
            }
            else
            {
                GraphicsHandler.D3DHandler.SetDefaultRasterState();
            }
            GraphicsHandler.D3DHandler.DeviceContext.DrawIndexed(Parts[partIndex].IndexCount, Parts[partIndex].StartIndex, 0);
        }

        /// <summary>
        /// Automatically preps the vertex and index buffers, then draws all the parts of the model.
        /// </summary>
        public void DrawAll()
        {
            /* Prep the vertex and index buffers */
            Begin();

            for (int i = 0; i < Parts.Count; i++)
            {
                GraphicsHandler.D3DHandler.DeviceContext.PixelShader.SetShaderResource(0, Parts[i].DiffuseMap);
                Draw(i);
            }
        }
        #endregion

        /// <summary>
        /// Adds a convex hull to render.
        /// </summary>
        /// <param name="d3dHandler">The D3Dhandler that will render the convex hull. </param>
        /// <param name="vertexData">The list of vertices of the convex hull. </param>
        public void AddConvexHull(D3DHandler d3dHandler, List<StaticVertex> vertexData)
        {
            int icount = Indices.Count;

            List<int> pseudoIndicex = GenerateIndexData(Indices[Indices.Count - 1]+1, vertexData.Count());

            Vertices.AddRange(vertexData);
            Indices.AddRange(pseudoIndicex);

            /* Stroe Parts */
            Parts.Add(new ConvexHull()
            {
                IndexCount = pseudoIndicex.Count,
                StartIndex = icount,
                DiffuseMap = Parts[Parts.Count - 1].DiffuseMap
            });


            VertexBuffer = Buffer11.Create<StaticVertex>(d3dHandler.Device, BindFlags.VertexBuffer, Vertices.ToArray());
            IndexBuffer = Buffer11.Create(d3dHandler.Device, BindFlags.IndexBuffer, Indices.ToArray());
            VertexSize = SharpDX.Utilities.SizeOf<StaticVertex>();

        }

        private void LoadModelFromFile(string filepath)
        {
            Parts = new List<GraphicsObjectPart>(); /* Subsets, parts of the model */

            /* Create Geometry to use to create a model */
            List<Geometry> geoms = objReader.CreateGeoms(filepath);

            Vertices = new List<StaticVertex>();
            Indices = new List<int>();

            int vcount = 0;
            int icount = 0;

            foreach (Geometry geom in geoms)
            {
                Vertices.AddRange(geom.VertexData);
                Indices.AddRange(geom.IndexData.Select(i => i + vcount));

                var mate = geom.MeshMaterial.First();


                ShaderResourceView tex = null;
                if (!string.IsNullOrEmpty(mate.DiffuseMap))
                {
                    string textureFile = System.IO.Path.GetDirectoryName(filepath) + "\\" + System.IO.Path.GetFileName(mate.DiffuseMap);
                    tex = ShaderResourceView.FromFile(Device, textureFile);
                }

                /* Stroe Parts */
                Parts.Add(new GraphicsObjectPart()
                {
                    IndexCount = geom.IndexData.Count,
                    StartIndex = icount,
                    DiffuseMap = tex
                });

                vcount += geom.VertexData.Count;
                icount += geom.IndexData.Count;
            }

            VertexBuffer = Buffer11.Create<StaticVertex>(Device, BindFlags.VertexBuffer, Vertices.ToArray());
            IndexBuffer = Buffer11.Create(Device, BindFlags.IndexBuffer, Indices.ToArray());
            VertexSize = SharpDX.Utilities.SizeOf<StaticVertex>();
        }

        /// <summary>
        /// Generates a list of int  represending the indices starting at chosen index up to chosen size. 
        /// </summary>
        /// <param name="startingIndex">The starting value of the output list. </param>
        /// <param name="size">The size of the output list. </param>
        /// <returns>Returns a list of incrementing value of size equal to the size parameter.</returns>
        public List<int> GenerateIndexData(int startingIndex, int size)
        {
            List<int> result = new List<int>();
            
            for (int i = startingIndex; i < startingIndex + size; ++i)
            {
                result.Add(i);
            }

            return result;
        }

        /// <summary>
        /// Finds the width of the model over the X-axis.
        /// </summary>
        /// <returns> Returns the width of the model as a float. </returns>
        public float GetWidthX()
        {
            float minX = float.MaxValue;
            float maxX = float.MinValue;
            foreach (StaticVertex vertex in Vertices)
            {
                if (vertex.Position.X < minX)
                {
                    minX = vertex.Position.X;
                }
                if (vertex.Position.X > maxX)
                {
                    maxX = vertex.Position.X;
                }
            }

            return maxX - minX;
        }

        /// <summary>
        /// Finds the width of the model over the Z-axis.
        /// </summary>
        /// <returns> Returns the width of the model as a float. </returns>
        public float GetWidthZ()
        {
            float minZ = float.MaxValue;
            float maxZ = float.MinValue;
            foreach (StaticVertex vertex in Vertices)
            {
                if (vertex.Position.Z < minZ)
                {
                    minZ = vertex.Position.Z;
                }
                if (vertex.Position.Z > maxZ)
                {
                    maxZ = vertex.Position.Z;
                }
            }

            return maxZ - minZ;
        }



        #region debug methods
        /// <summary>
        /// Prints the graphics object's position on console.
        /// </summary>
        public void PrintPosition()
        {
            Console.WriteLine("{0}, " + "{1}, " + "{2}", Position.X, Position.Y, Position.Z);
        }
        #endregion
    }
}
