﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using SharpDX.Windows;      /* RenderForm */
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;         /* Swap Chain Description, Mode Description, Samplel Description, Rational */

/* Renames due to overlapping clas names */
using Device11 = SharpDX.Direct3D11.Device;
using Buffer11 = SharpDX.Direct3D11.Buffer;

namespace NewtonLab
{
    class D3DHandler
    {
        private Device11 device;
        private SwapChain swapChain;
        private DeviceContext deviceContext;

        /* Renderin states */
        private RasterizerState rasterState;
        private BlendState blendState;
        private DepthStencilState depthState;
        private SamplerState samplerState;

        /* DirectX11 Components */
        public RenderForm Form { get; private set; }
        public Device11 Device { get { return device; } }
        public SwapChain SwapChain { get{return swapChain;} }
        public RenderTargetView BackBufferView { get; set; }
        public DepthStencilView DepthBufferView { get; set; }
        public DeviceContext DeviceContext { get { return deviceContext; } }
        public bool MustResize { get; private set; }

        public Texture2D backBufferTexture { get; set; }

        public SharpDX.DXGI.Surface1 surface { get; set; }

        public SharpDX.DXGI.Adapter1 adapter { get; set; }

        public D3DHandler(RenderForm renderForm)
        {
            /* Store the current render form */
            Form = renderForm;

            /* Initialize Device and Swap Chain */
            InitializeDeviceAndSwapChain();

            /* Get and store device context */
            deviceContext = Device.ImmediateContext;

            /* Ignore all windows events */
            var factory = SwapChain.GetParent<Factory>();
            factory.MakeWindowAssociation(Form.Handle, WindowAssociationFlags.IgnoreAll);

            /* Setup handler on resize form. Lambda expression basically just sets MustResize to true upon Form Resize */
            Form.UserResized += (sender, args) => MustResize = true;

            InitializeBackBufferView();

            InitializeDepthBuffer();

            SetDefaultTargets();

            /* Set Up Rendering States */
            SetDefaultRasterState();
            SetDefaultBlendState();
            SetDefaultDepthState();
            SetDefaultSamplerState();

        }

        /// <summary>
        /// Creates the DirectX11 device and swap chain used for rendering.
        /// </summary>
        private void InitializeDeviceAndSwapChain()
        {
            /* Create Swap Chain Description */
            SwapChainDescription swapChainDesc = new SwapChainDescription()
            {
                BufferCount = 2,
                Usage = Usage.RenderTargetOutput,
                OutputHandle = Form.Handle,
                IsWindowed = true,
                ModeDescription = new ModeDescription(0, 0, new Rational(60, 1), Format.R8G8B8A8_UNorm),
                SampleDescription = new SampleDescription(1, 0),
                Flags = SwapChainFlags.AllowModeSwitch,
                SwapEffect = SwapEffect.Discard
            };

            /* Set Feature Levels */
            FeatureLevel[] levels = new FeatureLevel[] { FeatureLevel.Level_11_0 };

            /* Create device and swap chain */
            /* Check if DirectX11 is supported by hardware */
           if (!IsDirectX11Supported())
            {
                DeviceCreationFlags flag = DeviceCreationFlags.BgraSupport;
                Device11.CreateWithSwapChain(SharpDX.Direct3D.DriverType.Warp, flag, levels, swapChainDesc, out device, out swapChain);
            }
            else
            {
                DeviceCreationFlags flag = DeviceCreationFlags.BgraSupport;              
                Device11.CreateWithSwapChain(SharpDX.Direct3D.DriverType.Hardware, flag, levels, swapChainDesc, out device, out swapChain);
            }
        }

        /// <summary>
        /// Initializes the view for manipulating the backbuffer.
        /// </summary>
        public void InitializeBackBufferView()
        {
            /* Get BackBuffer */
            backBufferTexture = SwapChain.GetBackBuffer<Texture2D>(0);

            /* Initialize BackBuffer View */
            BackBufferView = new RenderTargetView(Device, backBufferTexture);

            surface = backBufferTexture.QueryInterface<SharpDX.DXGI.Surface1>();
            /* Clean up backBufferTexture */
            backBufferTexture.Dispose();
        }

        /// <summary>
        /// Initializes the depth buffer.
        /// </summary>
        public void InitializeDepthBuffer()
        {
            /* Create zBufferTexture for initializes depth buffer view */
            Texture2D zbufferTexture = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.D16_UNorm,
                ArraySize = 1,
                MipLevels = 1,
                Width = Form.ClientSize.Width,
                Height = Form.ClientSize.Height,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            });

            /* Create depth buffer view */
            DepthBufferView = new DepthStencilView(Device, zbufferTexture);

            /* Clean up used texture */
            zbufferTexture.Dispose();
        }

        /// <summary>
        /// Resets/Initializes the Raster State.
        /// </summary>
        public void SetDefaultRasterState()
        {
            Utilities.Dispose(ref rasterState);
            RasterizerStateDescription rasterDescription = RasterizerStateDescription.Default();
            rasterState = new RasterizerState(Device, rasterDescription);
            DeviceContext.Rasterizer.State = rasterState;
        }

        /// <summary>
        /// Show the object as wireframe.
        /// </summary>
        public void SetWireframeRasterState()
        {
            Utilities.Dispose(ref rasterState);
            RasterizerStateDescription rasterDescription = RasterizerStateDescription.Default();
            rasterDescription.FillMode = FillMode.Wireframe;
            rasterState = new RasterizerState(Device, rasterDescription);
            DeviceContext.Rasterizer.State = rasterState;
        }

        /// <summary>
        /// Resets/Initializes the Blend State.
        /// </summary>
        public void SetDefaultBlendState()
        {
            Utilities.Dispose(ref blendState);
            BlendStateDescription description = BlendStateDescription.Default();
            blendState = new BlendState(Device, description);
        }

        /// <summary>
        /// Resets/Initializes the Depth State.
        /// </summary>
        public void SetDefaultDepthState()
        {
            Utilities.Dispose(ref  depthState);
            DepthStencilStateDescription description = DepthStencilStateDescription.Default();
            description.DepthComparison = Comparison.LessEqual;
            description.IsDepthEnabled = true;

            depthState = new DepthStencilState(Device, description);
        }

        /// <summary>
        /// Resets/Initializes the Sampler State.
        /// </summary>
        public void SetDefaultSamplerState()
        {
            Utilities.Dispose(ref samplerState);
            SamplerStateDescription description = SamplerStateDescription.Default();
            description.Filter = Filter.MinMagMipLinear;
            description.AddressU = TextureAddressMode.Wrap;
            description.AddressV = TextureAddressMode.Wrap;
            samplerState = new SamplerState(Device, description);
        }

        /// <summary>
        /// Updates the rendering states in the device context.
        /// </summary>
        public void UpdateAllStates()
        {
            DeviceContext.Rasterizer.State = rasterState;
            DeviceContext.OutputMerger.SetBlendState(blendState);
            DeviceContext.OutputMerger.SetDepthStencilState(depthState);
            DeviceContext.PixelShader.SetSampler(0, samplerState);
        }

        /// <summary>
        /// Sets the render and depth buffer of device context.
        /// </summary>
        public void SetDefaultTargets()
        {
            DeviceContext.Rasterizer.SetViewport(0, 0, Form.ClientSize.Width, Form.ClientSize.Height);
            DeviceContext.OutputMerger.SetTargets(DepthBufferView, BackBufferView);
        }

        /// <summary>
        /// Resizes the form and update rendering component sizes.
        /// </summary>
        public void Resize()
        {
            /* Dispose of previous views */
            BackBufferView.Dispose();
            DepthBufferView.Dispose();
            surface.Dispose();

            /* Resize the backbuffer */
            SwapChain.ResizeBuffers(1, Form.ClientSize.Width, Form.ClientSize.Height, Format.R8G8B8A8_UNorm, SwapChainFlags.AllowModeSwitch);

            InitializeBackBufferView();

            InitializeDepthBuffer();

            SetDefaultTargets();

            MustResize = false;
        }

        /// <summary>
        /// Sets the entire back buffer as a single color.
        /// </summary>
        /// <param name="color">The color to apply to the back buffer. </param>
        public void Clear(Color4 color)
        {
            DeviceContext.ClearRenderTargetView(BackBufferView, color);
            DeviceContext.ClearDepthStencilView(DepthBufferView, DepthStencilClearFlags.Depth, 1.0F, 0);
        }

        /// <summary>
        /// Present scene to screen.
        /// </summary>
        public void Present()
        {
            SwapChain.Present(0, PresentFlags.None);
        }

        /// <summary>
        /// Update constant buffer
        /// </summary>
        /// <typeparam name="T">Data Type</typeparam>
        /// <param name="buffer">Buffer to update</param>
        /// <param name="data">Data to write inside buffer</param>
        public void UpdateData<T>(Buffer11 buffer, T data) where T : struct
        {
            DeviceContext.UpdateSubresource(ref data, buffer);
        }

        /// <summary>
        /// Checks whether the hardware of host machine supports DirectX11.
        /// </summary>
        /// <returns> Returns true if hardware supports DirectX11. </returns>
        private bool IsDirectX11Supported()
        {
            return SharpDX.Direct3D11.Device.GetSupportedFeatureLevel() == FeatureLevel.Level_11_0;
        }
    }
}
