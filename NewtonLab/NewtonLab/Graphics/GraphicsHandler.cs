﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

/* SharpDX Libraries */
using SharpDX;
using SharpDX.Windows;  /* RenderForm */
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using FactoryDXGI = SharpDX.DXGI.Factory1;
using Buffer11 = SharpDX.Direct3D11.Buffer;
using Matrix11 = SharpDX.Matrix;
using Color11 = SharpDX.Color;

using NewtonLab.Graphics.Shaders;

using NewtonEngine;
using NewtonLab.Graphics.GUI;



namespace NewtonLab.Graphics
{
    class GraphicsHandler
    {
        /* Basic Shader Constant Buffer */
        private struct ShaderBufferInfo
        {
            public Matrix11 world;
            public Matrix11 worldViewProjection;
            public Vector4 lightDirection;
        }

        public D3DHandler D3DHandler {get; private set;}
        public GUIRenderer GUIRenderer { get; private set; } 
        private RenderForm renderForm;
        private BasicShader basicShader;
        private HighlightedShader highlightedShader;

        public Matrix11 Projection { get; private set; }

        /* Test Light Direction */
        Vector3 lightPosition = new Vector3(0.5f, 0.5f, -1);

        /* All the models to render */
        List<GraphicsObject> graphicsObjects = new List<GraphicsObject>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="e_renderForm">The form that the graphics handler will draw to. </param>
        public GraphicsHandler(RenderForm e_renderForm)
        {
            /* Store renderForm */
            this.renderForm = e_renderForm;

            /* Create Device */
            D3DHandler = new D3DHandler(e_renderForm);

            /* Create Basic Shader */
            basicShader = new BasicShader(D3DHandler, "./Shaders/BasicShader.hlsl",
                    new ShaderDescription() { VertexShaderFunction = "VS", PixelShaderFunction = "PS" },
                    new InputElement[] {  
                        new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                        new InputElement("NORMAL", 0, Format.R32G32B32_Float, 12, 0),
                        new InputElement("TEXCOORD", 0, Format.R32G32_Float, 24, 0)
                    });


            /* Create Highlight Shader */
            highlightedShader = new HighlightedShader(D3DHandler, "./Shaders/HighlightedShader.hlsl",
                new ShaderDescription() { VertexShaderFunction = "VS", PixelShaderFunction = "PS" },
                new InputElement[] {  
                    new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                    new InputElement("NORMAL", 0, Format.R32G32B32_Float, 12, 0),
                    new InputElement("TEXCOORD", 0, Format.R32G32_Float, 24, 0)
                });

            /* Create Projection */
            /* Set the rendering matrices */
            float ratio = (float)renderForm.ClientRectangle.Width / (float)renderForm.ClientRectangle.Height;
            Projection = Matrix11.PerspectiveFovLH(SharpDX.MathUtil.PiOverFour, ratio, 0.01F, 1000.0F);

            /* Initialize GUI Renderer */
            GUIRenderer = new GUIRenderer(this);
        }

        /// <summary>
        /// Update method for each frame.
        /// </summary>
        /// <param name="camera">The camera that will be used for rendering.</param>
        public void Frame(Camera camera)
        {
            /* If form has been resized, resize direct3D components */
            if (D3DHandler.MustResize)
            {
                D3DHandler.Resize();

                /* Set the rendering matrices */
                float ratio = (float)renderForm.ClientRectangle.Width / (float)renderForm.ClientRectangle.Height;
                Projection = Matrix11.PerspectiveFovLH(SharpDX.MathUtil.PiOverFour, ratio, 0.01F, 1000.0F);
            }
           
            /* Update all DX11 Device States */
            D3DHandler.UpdateAllStates();

            /* Draw background */
            D3DHandler.Clear(SharpDX.Color.CornflowerBlue);

            /* Render all game objects */
            RenderAllObjects(camera);

            /* Draw GUI Elements */
            GUIRenderer.Frame();

            /* Draw Backbuffer */
            D3DHandler.Present();            
        }

        /// <summary>
        /// Adds a graphics object to the list to be rendered.
        /// </summary>
        /// <param name="graphicsObject">The graphics object to add to the list to be rendered. </param>
        public void AddGraphicsObject(GraphicsObject graphicsObject)
        {
            graphicsObjects.Add(graphicsObject);
        }

        /// <summary>
        /// Removes a graphics object from the list to be rendered.
        /// </summary>
        /// <param name="graphicsObject">The graphics object to remove the list to be rendered. </param>
        public void RemoveGraphicsObject(GraphicsObject graphicsObject)
        {
            graphicsObjects.Remove(graphicsObject);
        }

        /// <summary>
        /// Render all the graphics objects. 
        /// </summary>
        private void RenderAllObjects(Camera camera)
        {
            /* List to destroy */
            List<GraphicsObject> toDestroy = new List<GraphicsObject>();

            /* Set up Matrices */
            Matrix11 view = camera.View;

            Matrix11 world;

            lightPosition.Normalize();

            /* Render all models */
            foreach (GraphicsObject graphicsObject in graphicsObjects)
            {
                /* Apply transformation from physics engine */
                world = new Matrix11(graphicsObject.Transformations);

                /* Scale matricies */

                /* Set up and Update Shaders */
                /* Fill in Shader Buffer */
                ShaderBufferInfo shaderBufferInfo = new ShaderBufferInfo()
                {
                    world = world,
                    worldViewProjection = world * view * Projection,
                    lightDirection = new Vector4(lightPosition, 1)
                };

                /* Create Shader Buffer */
                Buffer11 basicShaderBuffer = basicShader.CreateBuffer<ShaderBufferInfo>();
                Buffer11 hightLightedShaderBuffer = highlightedShader.CreateBuffer<ShaderBufferInfo>();

                if (graphicsObject.Highlighted)
                {
                    /* Fill Shader Buffer Info into the shader buffer */
                    D3DHandler.UpdateData<ShaderBufferInfo>(hightLightedShaderBuffer, shaderBufferInfo);

                    /* Set the constant buffers in shaders */
                    D3DHandler.DeviceContext.VertexShader.SetConstantBuffer(0, hightLightedShaderBuffer);
                    D3DHandler.DeviceContext.PixelShader.SetConstantBuffer(0, hightLightedShaderBuffer);

                    highlightedShader.SetShadersToDevice();
                }
                else
                {
                    /* Fill Shader Buffer Info into the shader buffer */
                    D3DHandler.UpdateData<ShaderBufferInfo>(basicShaderBuffer, shaderBufferInfo);

                    /* Set the constant buffers in shaders */
                    D3DHandler.DeviceContext.VertexShader.SetConstantBuffer(0, basicShaderBuffer);
                    D3DHandler.DeviceContext.PixelShader.SetConstantBuffer(0, basicShaderBuffer);

                    /* Set all the shaders */
                    basicShader.SetShadersToDevice();
                }
                

                graphicsObject.DrawAll();

                /* Dispose shader buffers */
                basicShaderBuffer.Dispose();
                hightLightedShaderBuffer.Dispose();
            }
        }
    }
}
