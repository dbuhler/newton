﻿using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.Toolkit.Graphics;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Point = System.Drawing.Point;

namespace NewtonLab.Graphics.GUI
{
    abstract class GUIObject
    {
        public Vector2 Position { get; set; }

        public String Text { get; set; }

        public static GraphicsHandler DefaultGraphicsHandler;
        public static RenderForm DefaultRenderForm;
        public GraphicsHandler GraphicsHandler { get; set; }
        public bool isShow { get; set; }

        protected RenderForm RenderForm;
        protected SpriteBatch Batch { get; private set; }
        protected PrimitiveBatch<VertexPositionColor> PrimitiveBatch { get; private set; }
        protected FontRenderer FontRenderer { get; private set; }
        protected ShaderResourceView Texture { get; set; }
       
        public bool Editable { get; set; }

        public GUIObject(string filename, RenderForm renderForm = null, GraphicsHandler gHandler = null)
        {
            this.isShow = true;
            GraphicsHandler = gHandler ?? DefaultGraphicsHandler;
            RenderForm = renderForm ?? DefaultRenderForm;

            FontRenderer = GraphicsHandler.GUIRenderer.FontRenderer;
            Batch = GraphicsHandler.GUIRenderer.Batch;

            PrimitiveBatch = GraphicsHandler.GUIRenderer.PrimitiveBatch;

            if (filename != null)
            {
                Texture = ShaderResourceView.FromFile(GraphicsHandler.D3DHandler.Device, filename);
            }
        }

        /// <summary>
        /// Gets the texture size, useful for drawing gui.
        /// </summary>
        /// <param name="tex">The texture for which its size is requested.</param>
        /// <returns>Returns a Vector2 containint the width and height of the texture.</returns>
        protected Vector2 GetTextureSize(ShaderResourceView tex)
        {
            using (var resource = tex.Resource)
            {
                using (var texture2D = resource.QueryInterface<SharpDX.Direct3D11.Texture2D>())
                    return new Vector2(texture2D.Description.Width, texture2D.Description.Height);
            }
        }

        /// <summary>
        /// Draws the GUI Object.
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Update the texture of the GUI Object.
        /// </summary>
        public abstract void UpdateTexture();
       
        /// <summary>
        /// Returns true if this GUI object has been clicked.
        /// </summary>
        /// <param name="position">The position clicked on the screen.</param>
        /// <returns>Returns true if this GUI object has been clicked.</returns>
        public abstract bool HandleInputs(Vector2 position);

        /// <summary>
        /// Display the GUI Object.
        /// </summary>
        public void Show()
        {
            this.isShow = true;
        }

        /// <summary>
        /// Hide the GUI Object.
        /// </summary>
        public void Hide()
        {
            this.isShow = false;
        }
    }
}
