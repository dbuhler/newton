﻿using SharpDX;
using SharpDX.Toolkit.Graphics;
using SharpDX.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewtonLab.Graphics.GUI
{
    class VectorPainter : GUIObject
    {
        #region Constants 

        /* Vector Colors */
        private Color XColor = SharpDX.Color.Green;
        private Color YColor = SharpDX.Color.Red;
        private Color ZColor = SharpDX.Color.Blue;

        /* Default Vectors */
        private Vector3 XVector = new Vector3(0.5f, 0, 0);
        private Vector3 YVector = new Vector3(0, 0.5f, 0);
        private Vector3 ZVector = new Vector3(0, 0, 0.5f);

        /* Vector Triangles */
        private Vector3 XTri1 = new Vector3(0.45f, -0.05f, 0);
        private Vector3 XTri2 = new Vector3(0.45f, 0.05f, 0);

        private Vector3 YTri1 = new Vector3(-0.05f, 0.45f, 0);
        private Vector3 YTri2 = new Vector3(0.05f, 0.45f, 0);

        private Vector3 ZTri1 = new Vector3(0, -0.05f, 0.45f);
        private Vector3 ZTri2 = new Vector3(0, 0.05f, 0.45f);

        #endregion

        public GameObject HeldObject { get; set; }

        public Camera Camera { get; private set; }

        Vector3 StartPoint;
        Vector3 EndPointX;
        Vector3 EndPointY;
        Vector3 EndPointZ;
        Matrix World;

        public VectorPainter(Camera camera, string filename = null, RenderForm renderForm = null, GraphicsHandler gHandler = null)
            : base(filename, renderForm, gHandler)
        {
            Camera = camera;
        }

        /// <summary>
        /// Draw the Vectors 
        /// </summary>
        public override void Draw()
        {
            if (HeldObject != null)
            {
                /* Build translation matrix */
                Matrix translationMatrix = Matrix.Identity;
                translationMatrix.Row4 = new Matrix(HeldObject.Transformations).Row4;

                /* Apply transformation from physics engine */
                World = translationMatrix;
                World = Matrix.Multiply(World, Camera.View);

                /* Set up vector points */
                StartPoint = World.TranslationVector;
                EndPointX = StartPoint + Vector3.TransformCoordinate(XVector, World);
                EndPointY = StartPoint + Vector3.TransformCoordinate(YVector, World);
                EndPointZ = StartPoint + Vector3.TransformCoordinate(ZVector, World);

                /* Draw the Axis Lines */
                DrawAxisLines();

                DrawTriangles();
            }
        }

        /// <summary>
        /// Draw the Axis Lines.
        /// </summary>
        private void DrawAxisLines()
        {
            /* Draw Vector Lines */
            PrimitiveBatch.DrawLine(new VertexPositionColor(StartPoint, XColor), new VertexPositionColor(EndPointX, XColor));
            PrimitiveBatch.DrawLine(new VertexPositionColor(StartPoint, YColor), new VertexPositionColor(EndPointY, YColor));
            PrimitiveBatch.DrawLine(new VertexPositionColor(StartPoint, ZColor), new VertexPositionColor(EndPointZ, ZColor));
        }

        /// <summary>
        /// Draw the axis vector pointers.
        /// </summary>
        private void DrawTriangles()
        {
            /* Draw X Triangle */
            Vector3 XTriPt1 = Vector3.TransformCoordinate(XTri1, World);
            Vector3 XTriPT2 = Vector3.TransformCoordinate(XTri2, World);

            VertexPositionColor XVPC1 = new VertexPositionColor(EndPointX, XColor);
            VertexPositionColor XVPC2 = new VertexPositionColor(StartPoint + XTriPt1, XColor);
            VertexPositionColor XVPC3 = new VertexPositionColor(StartPoint + XTriPT2, XColor);
            PrimitiveBatch.DrawTriangle(XVPC1, XVPC2, XVPC3);
            PrimitiveBatch.DrawTriangle(XVPC1, XVPC3, XVPC2);

            /* Draw Y Triangle */
            Vector3 YTriPt1 = Vector3.TransformCoordinate(YTri1, World);
            Vector3 YTriPT2 = Vector3.TransformCoordinate(YTri2, World);

            VertexPositionColor YVPC1 = new VertexPositionColor(EndPointY, YColor);
            VertexPositionColor YVPC2 = new VertexPositionColor(StartPoint + YTriPt1, YColor);
            VertexPositionColor YVPC3 = new VertexPositionColor(StartPoint + YTriPT2, YColor);
            PrimitiveBatch.DrawTriangle(YVPC1, YVPC2, YVPC3);
            PrimitiveBatch.DrawTriangle(YVPC1, YVPC3, YVPC2);

            /* Draw Z Triangle */
            Vector3 ZTriPt1 = Vector3.TransformCoordinate(ZTri1, World);
            Vector3 ZTriPT2 = Vector3.TransformCoordinate(ZTri2, World);

            VertexPositionColor ZVPC1 = new VertexPositionColor(EndPointZ, ZColor);
            VertexPositionColor ZVPC2 = new VertexPositionColor(StartPoint + ZTriPt1, ZColor);
            VertexPositionColor ZVPC3 = new VertexPositionColor(StartPoint + ZTriPT2, ZColor);
            PrimitiveBatch.DrawTriangle(ZVPC1, ZVPC2, ZVPC3);
            PrimitiveBatch.DrawTriangle(ZVPC1, ZVPC3, ZVPC2);

        }

        /// <summary>
        /// Show the original texture.
        /// </summary>
        public override void UpdateTexture()
        {

        }

        /// <summary>
        /// Returns true if this GUI object has been clicked.
        /// </summary>
        /// <param name="position">The screen point clicked.</param>
        /// <returns>Returns true if this GUI Object has been clicked.</returns>
        public override bool HandleInputs(Vector2 position)
        {
            return false;
        }

    }
}
